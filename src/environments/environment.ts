// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyAhdqA5iwojqVMw2j3o_G6-riaKa1hicPQ",
  authDomain: "isf-app-recap.firebaseapp.com",
  projectId: "isf-app-recap",
  storageBucket: "isf-app-recap.appspot.com",
  messagingSenderId: "264621768103",
  appId: "1:264621768103:web:a8fa26dd6bce9efc684a82"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
