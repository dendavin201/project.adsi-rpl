import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { MaterialDesign } from '../Material/material';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';
import { TentangComponent } from './tentang/tentang.component';


const routes: Routes =[
  {
    path:'',
    component:AdminComponent,
    children: [
      {
        path:'dashboard',
        component: DashboardComponent
      },
      {
        path:'product',
        component:ProductComponent
      },
      {
        path:'tentang',
        component:TentangComponent
      },
      {
        path : '',
        redirectTo: '/admin/dashboar',
        pathMatch:'full'
      }
    ]
  }
]

@NgModule({
  declarations: [
    DashboardComponent,
    ProductComponent,
    ProductDetailComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign,
    FormsModule
  ]
})
export class AdminModule { }
